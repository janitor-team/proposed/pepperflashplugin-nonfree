pepperflashplugin-nonfree (1.8.8~deb10u1) buster; urgency=medium

  * QA upload.
  * Rebuild for buster.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 30 Jan 2021 18:09:56 +0100

pepperflashplugin-nonfree (1.8.8) unstable; urgency=medium

  * QA upload.
  * Adobe Flash Player has reached End-of-Life and is no longer functional or
    available for download:
    https://www.adobe.com/products/flashplayer/end-of-life.html
  * Turn into a dummy package taking care of removing the previously installed
    plugin.  (Closes: #979689) (LP: #1911463)
  * Remove download and install functionality.
  * Remove chromium integration.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 15 Jan 2021 16:47:39 +0100

pepperflashplugin-nonfree (1.8.7) unstable; urgency=medium

  * QA upload.
  * Hard-code libgcc-s1 instead of libgcc1.

 -- Matthias Klose <doko@debian.org>  Thu, 18 Jun 2020 13:15:20 +0200

pepperflashplugin-nonfree (1.8.6) unstable; urgency=medium

  * QA upload.
  * binutils is dropped from Depends by accident

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 13 Apr 2020 00:31:51 +0800

pepperflashplugin-nonfree (1.8.5) unstable; urgency=medium

  * QA upload.
  * Remove most outdated libraries Depends, and move wget to Pre-Depends
    libpepflashplayer.so is almost static linked.
  * Use global wgetoptions when fetching upstream version
  * Fix lintian: package-uses-deprecated-debhelper-compat-version
  * Fix lintian: ancient-standards-version
  * Fix lintian: homepage-field-uses-insecure-uri
  * Fix lintian: rules-requires-root-missing
  * Fix lintian: file-contains-trailing-whitespace
  * Fix lintian: no-dep5-copyright

 -- Shengjing Zhu <zhsj@debian.org>  Mon, 13 Apr 2020 00:22:32 +0800

pepperflashplugin-nonfree (1.8.4) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group
  * Remove unused pubkey-google.txt
  * Update package description to reflect the current
    implementation (Closes: #912673)
  * Add i386 to package architecture (Closes: #886487)
  * Convert package to git and set Vcs-{Browser,Git} field
  * Use install command to copy files (Closes: #880710)
  * Fix wget using verbose option all the time (Closes: #858759)
  * Remove hal from Suggests, hal has been removed from archive

 -- Shengjing Zhu <zhsj@debian.org>  Fri, 15 Feb 2019 01:29:52 +0800

pepperflashplugin-nonfree (1.8.3+nmu1) unstable; urgency=medium

  [ Bob Proulx ]
  * Patched to use Adobe upstream rather than Google.
  * Inspired by the patch provided in Bug#833741#15 by Kristian Klausen
    but rewritten.  Closes: #833741,  #841373, #840779.
  * Adds in 32-bit support.

  [ Yann Dirson ]
  * Non-maintainer upload.
  * Updated Bob's patch to apply on 1.8.3

 -- Yann Dirson <dirson@debian.org>  Sat, 14 Jan 2017 18:57:25 +0100

pepperflashplugin-nonfree (1.8.3) unstable; urgency=medium

  * update-pepperflashplugin-nonfree:
    Added 'Dir::State::Status "/var/lib/dpkg/status";'.  Closes: #833643.
  * debian/control: Depends: gnupg1 | gnpug.

 -- Bart Martens <bartm@debian.org>  Sun, 07 Aug 2016 22:01:26 +0200

pepperflashplugin-nonfree (1.8.2+nmu1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update Google public key. Closes: #823005.
  * Remove 32 bit support. Closes: #816848.
  * Don't treat `apt-get update` warnings as errors. Closes: #818540.
  * Validate deb file with sha256sum.

 -- Kristian Klausen <klausenbusk@hotmail.com>  Fri, 13 May 2016 14:44:52 +0200

pepperflashplugin-nonfree (1.8.2) unstable; urgency=medium

  * Added APT::Sandbox::User "root"; Closes: #769740.
  * Added '|| true'. Closes: #803349.

 -- Bart Martens <bartm@debian.org>  Thu, 17 Dec 2015 17:36:19 +0100

pepperflashplugin-nonfree (1.8.1) unstable; urgency=medium

  * debian/control: Pre-Depends: ca-certificates.  Closes: #773629.

 -- Bart Martens <bartm@debian.org>  Sun, 21 Dec 2014 11:37:47 +0100

pepperflashplugin-nonfree (1.8) unstable; urgency=medium

  * Removed support for /etc/chromium/default.  Closes: #760388.
    + etc-chromium-default.txt: Removed.
    + debian/etc/chromium.d/pepperflashplugin-nonfree: Added.
    + debian/dirs: Added etc/chromium.d/.
    + debian/install: add file in etc/chromium.d/.
    + update-pepperflashplugin-nonfree: Removed dealing with
      /etc/chromium/default and /etc/chromium.d.
    + debian/control: Conflicts: chromium (<< 37.0.2062.120-4).  This package
      doesn't really conflict with these old versions of chromium, but it makes
      explicit that pepperflashplugin-nonfree isn't useful for versions of
      chromium still using /etc/chromium/default, as agreed on debian-release
      on Sun, 12 Oct 2014 15:38:03 +0200.

 -- Bart Martens <bartm@debian.org>  Wed, 22 Oct 2014 07:49:22 +0200

pepperflashplugin-nonfree (1.7) unstable; urgency=medium

  * Closes: #761397 :
    + etc-chromium-default.txt: Use same approach as in
      update-pepperflashplugin-nonfree for reading the flash version
      from the so file.
    + update-pepperflashplugin-nonfree: Replace contents of
      etc-chromium-default.txt in /etc/chromium/default when
      installing again.
    + Remove contents of etc-chromium-default.txt from /etc/chromium/default
      when uninstalling and when /etc/chromium.d exists.
  * update-pepperflashplugin-nonfree: Added removal of old deb files.
    Closes: #761323, #761495.

 -- Bart Martens <bartm@debian.org>  Sun, 14 Sep 2014 12:39:03 +0200

pepperflashplugin-nonfree (1.6) unstable; urgency=medium

  * update-pepperflashplugin-nonfree: Remove bad latest*txt files from cache,
    and don't cache nor use bad downloaded latest*txt files.  Closes: #761022.
  * update-pepperflashplugin-nonfree: Don't update /etc/chromium/default if
    /etc/chromium.d exists.  However, still updating /etc/chromium/default if
    /etc/chromium.d does not exist, so not yet closing #760388.
  * debian/control: Added Depends: ca-certificates.  Closes: #758609.
  * update-pepperflashplugin-nonfree: Use "id -u".  Closes: #756204.
  * update-pepperflashplugin-nonfree: Also remove json.  Closes: #758081.

 -- Bart Martens <bartm@debian.org>  Wed, 10 Sep 2014 06:40:27 +0200

pepperflashplugin-nonfree (1.5) unstable; urgency=low

  * update-pepperflashplugin-nonfree: Added chrome-beta directory.
  * update-pepperflashplugin-nonfree: Install manifest.json. Closes: #753400.

 -- Bart Martens <bartm@debian.org>  Fri, 11 Jul 2014 08:27:39 +0200

pepperflashplugin-nonfree (1.4) unstable; urgency=medium

  * Additionally use /etc/chromium.d/. Closes: #752286.

 -- Bart Martens <bartm@debian.org>  Sun, 22 Jun 2014 12:25:16 +0200

pepperflashplugin-nonfree (1.3) unstable; urgency=medium

  * Depends: libpango-1.0-0 | libpango1.0-0, to make it work for stable.

 -- Bart Martens <bartm@debian.org>  Wed, 15 Jan 2014 21:28:50 +0100

pepperflashplugin-nonfree (1.2) unstable; urgency=low

  * Add arch in sources.list.  Closes: #727813.
  * Replaced libpango1.0-0 by libpango-1.0-0 in Depends.  Closes: #731493.

 -- Bart Martens <bartm@debian.org>  Sat, 21 Dec 2013 19:59:06 +0100

pepperflashplugin-nonfree (1.1) unstable; urgency=low

  * Look in both /chrome/ and /chrome-unstable/ for the sofile.
  * No longer "Conflicts: flashplayer-mozilla".  Closes: #724572.
  * Update /etc/chromium/default only when it exists.
  * Moving from experimental to unstable.

 -- Bart Martens <bartm@debian.org>  Sat, 28 Sep 2013 07:43:13 +0000

pepperflashplugin-nonfree (1) experimental; urgency=low

  * Initial package.  Closes: #715245.

 -- Bart Martens <bartm@debian.org>  Sun, 07 Jul 2013 22:52:06 +0200
